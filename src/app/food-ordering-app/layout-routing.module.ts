import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {LayoutComponent} from "./layout.component";
import {AuthGuard} from "../lib/services/auth-guard.service";

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {path: '', redirectTo: 'foods'},
      {path: 'foods', loadChildren: './user-page/sa.module#SAModule'},
      {path: 'login', loadChildren: './login/login.module#LoginModule'},
      {path: 'adminpanel', loadChildren: './admin-page/admin.module#AdminModule', canLoad: [AuthGuard]},
      {path: '**', redirectTo: 'foods'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule {
}
