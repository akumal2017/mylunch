export class OrderStatus {
  static NEW: string = "NEW";
  static DELIVERED: string = "DELIVERED";
  static CANCELED: string = "CANCELED";
  static PAID: string = "PAID";
  static IN_PROGRESS: string = "IN_PROGRESS";

}
