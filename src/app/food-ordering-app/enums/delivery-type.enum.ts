export class DeliveryType {
  static HOME_DELIVERY: string = "HOME_DELIVERY";
  static PICK_WHEN_READY: string = "PICK_WHEN_READY";
}
