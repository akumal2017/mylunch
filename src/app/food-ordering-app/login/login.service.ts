import {Injectable} from "@angular/core";
import {FTBaseService} from "../../lib/services/ft-base.service";
import {HttpService} from "../../lib/services/http.service";
import {SessionStorageService} from "../../lib/services/session-storage.service";
@Injectable()
export class LoginService extends FTBaseService {
  // dataModel: FoodModel = new FoodModel();

  serviceApi: string = '/user-management';
  authUrl: string = '/auth';
  changePwdUrl: string = '/chhangepassword';

  constructor(httpService: HttpService, private _sessionStorageService: SessionStorageService) {
    super(httpService);

  }

  authenticate(data) {
    return this.httpService.postRequest(this.serviceApi + this.authUrl, data);
  }

  logout() {
    this._sessionStorageService.clearSession();
    let baseHref = window.location.origin;
    window.location.href = baseHref + "/login";
  }

  changePassword(data) {
    return this.httpService.postRequest(this.serviceApi + this.changePwdUrl, data);
  }


}
