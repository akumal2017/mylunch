import {Component, OnInit, NgZone} from "@angular/core";
import {LoginService} from "./login.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ResponseModel} from "../../lib/model/response.model";
import {SessionStorageService} from "../../lib/services/session-storage.service";
import {Router} from "@angular/router";
@Component({
  selector: 'app-admin-login',
  templateUrl: 'login.component.html',
  styleUrls: ['helper.login.css', 'login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginErrorMsg: string;
  enableLoginBtn: boolean;

  constructor(private _formBuilder: FormBuilder,
              private _loginService: LoginService,
              private _sessionStorageService: SessionStorageService,
              private _router: Router,
              private _zone: NgZone) {
    this.enableLoginBtn = false;
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    // this.dataModel = (this.foodModel == null)? new FoodModel(): this.foodModel;

    this.loginForm = new FormGroup({});
    this.loginForm = this._formBuilder.group({
      userName: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  onSubmit() {
    let that = this;
    if (this.loginForm.valid) {
      this.enableLoginBtn = false;
      this._loginService.authenticate(this.loginForm.value).then((res: ResponseModel) => {
        if (res.responseStatus) {
          this._sessionStorageService.setToken(res.result);
          let baseHref = window.location.origin;
          window.location.href = baseHref + "/adminpanel";

        } else {
          this.loginErrorMsg = res.message;
          this.enableLoginBtn = true;
        }
      });
    }
  }

  clearError() {
    this.loginErrorMsg = null;
  }

  disableBtn() {
    if (this.loginForm.invalid && !this.enableLoginBtn) {
      return true;
    } else {
      return false;
    }
  }


}
