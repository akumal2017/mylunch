import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {LoginRoutingModule} from "./login-routing.module";
import {LoginComponent} from "./login.component";
import {LoginService} from "./login.service";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LoginRoutingModule

  ],
  declarations: [
    LoginComponent,
  ],
  providers: [LoginService]
})
export class LoginModule {
}
