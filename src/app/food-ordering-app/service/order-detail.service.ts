import {Injectable} from "@angular/core";
import {FTBaseService} from "../../lib/services/ft-base.service";
import {OrderModel} from "../model/order.model";
import {HttpService} from "../../lib/services/http.service";
/**
 * Created by admin on 5/26/2018.
 */
@Injectable()
export class OrderDetailService extends FTBaseService {
  dataModel: OrderModel = new OrderModel();

  serviceApi: string = '/mylunch/orderdetail';


  // private role:string=ApiConstant.USER_MANAGEMENT +'/role-module';

  constructor(httpService: HttpService) {
    super(httpService);
  }

  getDetailsByOrderId(orderId: number) {
    return this.httpService.getRequest(this.serviceApi + '/order' + orderId);
  }


}
