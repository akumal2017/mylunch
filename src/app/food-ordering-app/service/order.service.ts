import {Injectable} from "@angular/core";
import {FTBaseService} from "../../lib/services/ft-base.service";
import {OrderModel} from "../model/order.model";
import {HttpService} from "../../lib/services/http.service";
/**
 * Created by admin on 5/26/2018.
 */
@Injectable()
export class OrderService extends FTBaseService {
  dataModel: OrderModel = new OrderModel();

  serviceApi: string = '/mylunch/orderfood';


  // private role:string=ApiConstant.USER_MANAGEMENT +'/role-module';

  constructor(httpService: HttpService) {
    super(httpService);
  }

  updateByOrderStatus(updateOrder) {
    return this.httpService.putRequest(this.serviceApi + '/orderstatus', updateOrder);
  }

  newOrder(data) {
    return this.httpService.postRequest(this.serviceApi + '/neworder', data);
  }

  closeSales() {
    return this.httpService.getRequest(this.serviceApi + '/closesales');
  }

}
