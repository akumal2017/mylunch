import {BaseFtModel} from "../../lib/model/base-ft.model";
/**
 * Created by admin on 5/26/2018.
 */
export class OrderModel extends BaseFtModel {
  orderBy: string;
  contactNumber: string;
  deliveryAddress: string;
  deliveryType: string;
  totalAmount: number;
  foodOrderDetailList = [];
}
