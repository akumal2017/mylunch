/**
 * Created by admin on 5/22/2018.
 */
export class InternalRouteConstant {
  public static ORDER_ROUTE = "/foods/order";
  public static USER_LANDING_PAGE_ROUTE = "/foods/dashboard";
  public static ADMIN_DASHBOARD_ROUTE = "/adminpanel/dashboard";
  public static MANAGE_ORDER_ROUTE = "/adminpanel/manage-order";
}
