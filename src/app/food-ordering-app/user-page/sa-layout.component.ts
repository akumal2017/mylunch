import {OnInit, Component, OnDestroy} from "@angular/core";
import {Router} from "@angular/router";
import {InternalRouteConstant} from "../constant/internal-route.constant";
import {EventService} from "../components/service/event.service";
import * as jQuery from "jquery";
/**
 * Created by admin on 4/15/2018.
 */
@Component({
  selector: 'app-layout',
  templateUrl: 'sa-layout.component.html'
})
export class SALayoutComponent implements OnInit,OnDestroy {
  badgeCount: number = 0;
  subscribeEvent: any;

  constructor(private _router: Router, private _eventService: EventService) {

  }


  ngOnInit(): void {
    this.subscribeEvent = this._eventService.getCartNotification().subscribe((totalOrder: number) => {
      this.badgeCount = totalOrder;
    });
  }

  ngOnDestroy(): void {
    this.subscribeEvent.unsubscribe();
  }

  onLogoClick() {
    this._router.navigate([InternalRouteConstant.USER_LANDING_PAGE_ROUTE]);
  }

  onCartClick() {
    this._router.navigate([InternalRouteConstant.ORDER_ROUTE]);
  }

  hideSideNavBar() {
    jQuery("#app-drawer").removeClass('is-visible').attr('aria-hidden', true);
    jQuery(".mdl-layout__drawer-button").attr('aria-expanded', false);
    jQuery(".mdl-layout__obfuscator").removeClass('is-visible');

  }

}
