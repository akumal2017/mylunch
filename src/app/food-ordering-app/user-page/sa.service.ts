import {Injectable} from "@angular/core";
import {EventService} from "../components/service/event.service";
/**
 * Created by admin on 5/26/2018.
 */
@Injectable()
export class SAService {
  constructor(private _eventService: EventService) {

  }

  foodOrderList = [];

  addToFoodOrderList(foodOrder) {
    this.foodOrderList.push(foodOrder);
    this._eventService.setCartNotification(this.foodOrderList.length);
  }

  removeOrderFromList(foodOrder) {
    let index = this.foodOrderList.indexOf(foodOrder);
    this.foodOrderList.splice(index, 1);
    this._eventService.setCartNotification(this.foodOrderList.length);
  }

  getOrderDetailList() {
    return this.foodOrderList;
  }

  clearOrderList() {
    this.foodOrderList = [];
    this._eventService.setCartNotification(this.foodOrderList.length);
  }

}
