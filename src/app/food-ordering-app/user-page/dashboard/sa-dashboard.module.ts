import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SADashboardComponent} from "./sa-dashboard.component";
import {SADashboardRoutingModule} from "./sa-dashboard-routing.module";
import {FoodService} from "../../admin-page/food/food.service";
import {CustomPrimeNGModule} from "../../components/module/custom-primeng.module";


@NgModule({
  imports: [
    CommonModule,
    SADashboardRoutingModule,
    CustomPrimeNGModule
  ],
  declarations: [
    SADashboardComponent,
  ],

  providers: [FoodService]
})
export class SADashboardModule {
}
