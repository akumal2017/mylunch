import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {SADashboardComponent} from "./sa-dashboard.component";

const routes: Routes = [
  {
    path: '', component: SADashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SADashboardRoutingModule {
}
