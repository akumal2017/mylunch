import {FoodModel} from "../../admin-page/food/food.model";
/**
 * Created by admin on 5/26/2018.
 */
export class FoodOrderDetailModel extends FoodModel {
  quantity: number = 0;
}
