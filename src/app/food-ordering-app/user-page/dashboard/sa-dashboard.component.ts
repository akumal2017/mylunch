import {Component, OnInit} from "@angular/core";
import {FoodService} from "../../admin-page/food/food.service";
import {ResponseModel} from "../../../lib/model/response.model";
import {FoodOrderDetailModel} from "./food-order-detail.model";
import {SAService} from "../sa.service";
import {Message} from "primeng/components/common/message";
import {ApiConstant} from "../../../lib/constants/api.constant";

@Component({
  selector: 'app-dashboard',
  templateUrl: 'sa-dashboard.component.html',
})
export class SADashboardComponent implements OnInit {
  foodList: FoodOrderDetailModel[] = [];

  orderDetailList = [];
  msgs: Message[] = [];

  private stompClient;
  private serverUrl = ApiConstant.API_ROOT_URL + '/socket';

  constructor(private _foodService: FoodService, private _saService: SAService) {
  }
  ngOnInit() {
    this.getAll();

  }


  getAll() {
    this._foodService.getList().then((res: ResponseModel) => {
      if (res.responseStatus) {
        this.foodList = res.result;
      } else {
        this.foodList = [];
      }
    });
  }

  addQuantity(selectedFood) {
    selectedFood.quantity++;
  }

  minusQuantity(selectedFood) {
    if (selectedFood.quantity > 0) {
      selectedFood.quantity--;
    }
  }

  addToOrder(selectedFood) {
    if (selectedFood.quantity > 0) {
      let ofoFoodEntity = {id: selectedFood.id, name: selectedFood.name, price: selectedFood.price};
      let orderFood = {ofoFoodEntity: ofoFoodEntity, quantity: selectedFood.quantity};
      this._saService.addToFoodOrderList(orderFood);
      selectedFood.quantity = 0;
    } else {
      this.msgs.push({severity: 'error', summary: 'Error', detail: 'Quantity of Food must be greater than zero.'});
    }

  }

  getImage(food) {
    return ApiConstant.IMAGE_DISPLAY + food.image;
  }


}
