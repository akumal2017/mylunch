import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FoodOrderRoutingModule} from "./food-order-routing.module";
import {FoodOrderComponent} from "./food-order.component";
import {CustomMaterialModule} from "../../components/module/custom-material.module";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {ConfirmationDialogComponent} from "../../components/confirmation/confirmation-dialog.component";


@NgModule({
  imports: [
    CommonModule,
    FoodOrderRoutingModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule

  ],
  declarations: [
    FoodOrderComponent,
    ConfirmationDialogComponent
  ],
  entryComponents: [ConfirmationDialogComponent]
})
export class FoodOrderModule {
}
