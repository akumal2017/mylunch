import {Component, OnInit} from "@angular/core";
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {MatStepper, MatDialog} from "@angular/material";
import {ConfirmationDialogComponent} from "../../components/confirmation/confirmation-dialog.component";
import {SAService} from "../sa.service";
import {DeliveryType} from "../../enums/delivery-type.enum";
import {OrderService} from "../../service/order.service";
import {ResponseModel} from "../../../lib/model/response.model";
import * as Stomp from "stompjs";
import * as SockJS from "sockjs-client";
import {ApiConstant} from "../../../lib/constants/api.constant";

@Component({
  selector: 'app-dashboard',
  templateUrl: 'food-order.component.html',
  styleUrls: ['food-order.component.css']
})
export class FoodOrderComponent implements OnInit {
  isLinear = true;
  isEditable: boolean = true;
  isCompleted: boolean = false;
  foodOrderFormGroup: FormGroup;
  orderDetailList = [];
  grandTotalAmount: number;
  homeDelivery: boolean = false;
  isEmptyOrders: boolean;
  finalOrder: any = {};
  deliveryType: string = DeliveryType.PICK_WHEN_READY;
  finalizedMsg: string = "Waiting for your confirmation...";

  private serverUrl = ApiConstant.API_ROOT_URL + '/socket';
  private stompClient;

  // @ViewChild('stepper') stepper:MatStepper;
  constructor(private _formBuilder: FormBuilder,
              public dialog: MatDialog,
              private _saService: SAService,
              private _orderService: OrderService) {
  }

  ngOnInit() {
    this.initForm();
    this.orderDetailList = this._saService.getOrderDetailList();
    this.updateIsEmptyList();
    this.calulateGrandTotal();
    this.initWebSocketConnection();
  }

  initWebSocketConnection() {
    let that = this;
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);

    ws.onclose = function () {
      console.debug("Trying to reconnect...")
      setTimeout(() => {
        that.initWebSocketConnection();
      }, 5000);

    };
  }

  sendMessage(message) {
    this.stompClient.send("/app/send/order", {}, message);
  }

  initForm() {
    this.foodOrderFormGroup = this._formBuilder.group({
      orderBy: ['', Validators.required],
      contactNumber: ['', Validators.required],
      deliveryAddress: ''
    });
  }

  deliveryAddressValidator(control: FormControl) {
    let deliveryAddress: string = control.value;
    if (deliveryAddress !== undefined) {
      if (deliveryAddress.trim().length <= 0) {
        return {emptyAddress: true};
      }
    }
    return null;

  }

  updateIsEmptyList() {
    this.isEmptyOrders = this.orderDetailList.length == 0;
  }

  onSelectionChange(stepper: MatStepper) {
    if (stepper.selectedIndex == 0) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        disableClose: true,
        hasBackdrop: true
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this._orderService.newOrder(this.finalOrder).then((res: ResponseModel) => {
            if (res.responseStatus) {
              let resOrder = res.result;
              this.isCompleted = true;
              this.finalizedMsg = "Your Order has been submitted. Please bring your order ID, when you pick up your order. New order ID is " + resOrder.code;
              this.isEditable = false;
              this.sendMessage(JSON.stringify(resOrder));
              this._saService.clearOrderList();
            } else {
              stepper.previous();
            }
          });

        } else {
          stepper.previous();
        }
      });
    }
  }

  removeOrder(foodOrder) {
    this._saService.removeOrderFromList(foodOrder);
    this.orderDetailList = this._saService.getOrderDetailList();
    this.calulateGrandTotal();
    this.updateIsEmptyList();
  }

  calulateGrandTotal() {
    this.grandTotalAmount = 0;
    for (let i = 0; i < this.orderDetailList.length; i++) {
      let eachFoodOrder = this.orderDetailList[i];
      this.grandTotalAmount = this.grandTotalAmount + (eachFoodOrder.ofoFoodEntity.price * eachFoodOrder.quantity);
    }
  }

  onSelectDeliveryType(event) {
    this.deliveryType = event.value;
    if (event.value == DeliveryType.HOME_DELIVERY) {
      this.homeDelivery = true;
      this.addValidation();
    } else {
      this.homeDelivery = false;
      this.foodOrderFormGroup.controls['deliveryAddress'].setValue('');
      this.removeValidation();
    }
  }

  onOrderSubmit(event) {
    if (this.foodOrderFormGroup.valid) {
      this.finalOrder = this.foodOrderFormGroup.value;
      this.finalOrder.deliveryType = this.deliveryType;
      this.finalOrder.foodOrderDetailList = this.orderDetailList;
      this.finalOrder.totalAmount = this.grandTotalAmount;
    }
  }

  addValidation() {
    this.foodOrderFormGroup.controls['deliveryAddress'].setValidators([this.deliveryAddressValidator])
    this.foodOrderFormGroup.controls['deliveryAddress'].updateValueAndValidity()
  }

  removeValidation() {
    this.foodOrderFormGroup.controls['deliveryAddress'].clearValidators();
    this.foodOrderFormGroup.controls['deliveryAddress'].updateValueAndValidity()
  }

  keyPress(event: any) {
    const pattern = /[0-9]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}



