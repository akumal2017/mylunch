import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {FoodOrderComponent} from "./food-order.component";

const routes: Routes = [
  {
    path: '', component: FoodOrderComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FoodOrderRoutingModule {
}
