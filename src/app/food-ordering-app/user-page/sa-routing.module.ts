import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {SALayoutComponent} from "./sa-layout.component";

const routes: Routes = [
  {
    path: '',
    component: SALayoutComponent,
    children: [
      {path: '', redirectTo: 'dashboard'},
      {path: 'dashboard', loadChildren: './dashboard/sa-dashboard.module#SADashboardModule'},
      {path: 'order', loadChildren: './order/food-order.module#FoodOrderModule'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule {
}
