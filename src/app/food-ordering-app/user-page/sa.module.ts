import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {LayoutRoutingModule} from "./sa-routing.module";
import {SALayoutComponent} from "./sa-layout.component";
import {CommonComponentModule} from "../components/common.component";
import {CustomMaterialModule} from "../components/module/custom-material.module";
import {EventService} from "../components/service/event.service";
import {SAService} from "./sa.service";
import {OrderService} from "../service/order.service";


@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    CommonComponentModule,
    CustomMaterialModule

  ],
  declarations: [SALayoutComponent],
  providers: [EventService, SAService, OrderService]
})
export class SAModule {
}
