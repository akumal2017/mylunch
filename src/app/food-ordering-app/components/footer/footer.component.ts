import {Component, OnInit, Inject} from "@angular/core";
import {DOCUMENT} from "@angular/common";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {
  constructor(@Inject(DOCUMENT) private document: Document) {
  }

  ngOnInit(): void {
  }

  //todo fix me to scroll to top
  gotoTop() {
    this.document.body.scroll(0, 0);
  }
}
