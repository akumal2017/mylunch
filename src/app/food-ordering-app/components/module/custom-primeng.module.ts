import {NgModule} from "@angular/core";
import {TableModule} from "primeng/table";
import {PaginatorModule} from "primeng/paginator";
import {InputTextModule} from "primeng/inputtext";
import {InputTextareaModule} from "primeng/inputtextarea";
import {RadioButtonModule} from "primeng/radiobutton";
import {DropdownModule} from "primeng/dropdown";
import {ButtonModule} from "primeng/button";
import {SplitButtonModule} from "primeng/splitbutton";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {ConfirmationService} from "primeng/api";
import {DialogModule} from "primeng/dialog";
import {TooltipModule} from "primeng/tooltip";
import {OverlayPanelModule} from "primeng/overlaypanel";
import {CheckboxModule} from "primeng/checkbox";
import {MessagesModule} from "primeng/messages";
import {MessageModule} from "primeng/message";
import {GrowlModule} from "primeng/growl";
import {KeyFilterModule} from "primeng/components/keyfilter/keyfilter";
import {ProgressBarModule} from "primeng/components/progressbar/progressbar";
import {PasswordModule} from "primeng/components/password/password";
/**
 * Created by admin on 5/23/2018.
 */
@NgModule({
  imports: [
    TableModule,
    PaginatorModule,
    InputTextModule,
    InputTextareaModule,
    RadioButtonModule,
    DropdownModule,
    ButtonModule,
    SplitButtonModule,
    ConfirmDialogModule,
    DialogModule,
    TooltipModule,
    OverlayPanelModule,
    CheckboxModule,
    MessagesModule,
    MessageModule,
    GrowlModule,
    KeyFilterModule,
    ProgressBarModule,
    PasswordModule
  ],
  providers: [ConfirmationService],
  exports: [
    TableModule,
    PaginatorModule,
    InputTextModule,
    InputTextareaModule,
    RadioButtonModule,
    DropdownModule,
    ButtonModule,
    SplitButtonModule,
    ConfirmDialogModule,
    DialogModule,
    TooltipModule,
    OverlayPanelModule,
    CheckboxModule,
    MessagesModule,
    MessageModule,
    GrowlModule,
    KeyFilterModule,
    ProgressBarModule,
    PasswordModule
  ]

})
export class CustomPrimeNGModule {
}
