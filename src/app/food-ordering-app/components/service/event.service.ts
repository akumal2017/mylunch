import {Injectable} from "@angular/core";
import {Subject, Observable} from "rxjs";
/**
 * Created by admin on 5/26/2018.
 */

@Injectable()
export class EventService {
  private cartNotification: Subject<any> = new Subject<any>();

  setCartNotification(totalOrders: number): void {
    this.cartNotification.next(totalOrders);
  }

  getCartNotification(): Observable<any> {
    return this.cartNotification.asObservable();
  }

}
