import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AdminLayoutRoutingModule} from "./admin-routing.module";
import {AdminLayoutComponent} from "./admin-layout.component";
import {CommonComponentModule} from "../components/common.component";
import {OrderService} from "../service/order.service";
import {CustomPrimeNGModule} from "../components/module/custom-primeng.module";
import {OrderDetailService} from "../service/order-detail.service";
import {LoginService} from "../login/login.service";
import {ChangePasswordFormComponent} from "./change-password/change-password-form.component";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";


@NgModule({
  imports: [
    CommonModule,
    AdminLayoutRoutingModule,
    CommonComponentModule,
    CustomPrimeNGModule,
    FormsModule,
    ReactiveFormsModule

  ],
  declarations: [AdminLayoutComponent, ChangePasswordFormComponent],
  providers: [OrderService, OrderDetailService, LoginService]
})
export class AdminModule {
}
