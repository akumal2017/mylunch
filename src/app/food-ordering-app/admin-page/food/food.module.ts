import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {CustomMaterialModule} from "../../components/module/custom-material.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CustomPrimeNGModule} from "../../components/module/custom-primeng.module";
import {FoodFormComponent} from "./form/food-form.component";
import {FoodService} from "./food.service";
import {FtLibModule} from "../../../lib/ft-lib.module";
import {FoodListComponent} from "./list/food-list.component";
import {FoodRoutingModule} from "./food-routing.module";
import {ConfirmationService} from "primeng/components/common/confirmationservice";

@NgModule({
  imports: [
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CustomPrimeNGModule,
    FtLibModule,
    FoodRoutingModule

  ],
  declarations: [
    FoodFormComponent,
    FoodListComponent
  ],

  providers: [FoodService, ConfirmationService]

})
export class FoodModule {
}
