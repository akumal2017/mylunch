import {Component, Input} from "@angular/core";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {PrimeNGFormComponentBase} from "../../../../lib/components/base-form/primng-form-base.component";
import {FoodService} from "../food.service";
import {FoodModel} from "../food.model";
import {ResponseModel} from "../../../../lib/model/response.model";
import {FormAction} from "../../../../lib/enums/form-action.enum";
import {ApiConstant} from "../../../../lib/constants/api.constant";

@Component({
  selector: 'app-food-form',
  templateUrl: 'food-form.component.html',
  styleUrls: ['food-form.component.css']
})
export class FoodFormComponent extends PrimeNGFormComponentBase {
  @Input("foodModel")
  foodModel: FoodModel;
  @Input("buttonType")
  buttonType: string = this.ADD_BUTTON_ROLE;
  ofoFoodForm: FormGroup;
  imgUrl: string;
  progress = {value: 0};
  imageFile: File;

  isVisible: boolean = false;
  fileName: string;
  showFileUploadErrMsg: boolean = false;

  constructor(protected _formBuilder: FormBuilder, private _foodService: FoodService) {
    super();
    // this.foodModel = new FoodModel();
  }


  initForm() {
    // this.dataModel = (this.foodModel == null)? new FoodModel(): this.foodModel;

    this.ofoFoodForm = new FormGroup({});
    this.ofoFoodForm = this._formBuilder.group({
      id: this.foodModel.id,
      name: [this.foodModel.name, [Validators.required]],
      price: [this.foodModel.price, [Validators.required]],
      code: [{value: this.foodModel.code, disabled: true}],
      image: this.foodModel.image,
      description: this.foodModel.description,
      version: this.foodModel.version
    });
    this.baseForm = this.ofoFoodForm;
  }

  getService() {
    return this._foodService;
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.isVisible = true;
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      this.imageFile = event.target.files[0]

      reader.onload = (event: FileReaderEvent) => { // called once readAsDataURL is completed
        this.imgUrl = event.target.result;
      }
    } else {
      this.resetFileUpload();
    }
  }

  uploadImage() {
    this.getService().progress$.subscribe(
      (data: number) => {
        this.progress.value = data;
      });
    this.getService().uploadRequest(this.imageFile).subscribe((res: ResponseModel) => {
      if (res.responseStatus) {
        this.fileName = res.result;
        this.showFileUploadErrMsg = false;
      }
    });
  }

  add(dialogStatus) {
    // this.role = this.baseForm.value;
    if (this.fileName) {
      this.dataModel = this.baseForm.value;
      this.dataModel['image'] = this.fileName;
      this.getService().add(this.dataModel).then(data => {
        if (data.responseStatus) {
          let formAction = (dialogStatus === true) ? FormAction.SAVE_AND_NEW : FormAction.SAVE;
          this.emitDialogStatus(dialogStatus, formAction, data);
          this.baseForm.reset();
          this.resetFileUpload();
        }
        else {
          //this.alertModel.alertType = AlertType.ERROR;
          console.error("Form Submit Error");
        }
      });
    }
    else {
      this.showFileUploadErrMsg = true;
    }
  }

  update(dialogStatus) {
    this.dataModel = {};
    this.dataModel = this.baseForm.value;
    if (this.fileName) {
      this.dataModel['image'] = this.fileName;
    }
    this.getService().update(this.dataModel).then(data => {
      if (data.responseStatus) {
        this.emitDialogStatus(dialogStatus, FormAction.UPDATE, data);
        // this.getService().alertModel.alertType = AlertType.SUCCESS;
        this.getService().buttonRole = this.ADD_BUTTON_ROLE;
        this.resetFileUpload();
        //this.viewList();
      }
      else {
        console.error("Form update error");
        // this.alertModel.showAlert = true;
        // this.alertModel.message = data.message;
        // this.alertModel.alertType = AlertType.ERROR;
      }
    });
  }

  resetFileUpload() {
    this.isVisible = false;
    this.fileName = "";
    this.imageFile = null;
    this.progress.value = 0;
    this.imgUrl = "";
  }

  getImage() {
    return ApiConstant.IMAGE_DISPLAY + this.foodModel.image;
  }



}

interface FileReaderEventTarget extends EventTarget {
  result: string
}

interface FileReaderEvent extends Event {
  target: FileReaderEventTarget;
  getMessage(): string;
}



