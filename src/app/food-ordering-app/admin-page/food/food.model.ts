import {BaseFtModel} from "../../../lib/model/base-ft.model";
/**
 * Created by admin on 5/23/2018.
 */
export class FoodModel extends BaseFtModel {
  code: string;
  image: string;
  price: number;
  description: string;
}
