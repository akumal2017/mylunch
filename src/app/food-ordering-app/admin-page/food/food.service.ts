import {Injectable} from "@angular/core";
import {FTBaseService} from "../../../lib/services/ft-base.service";
import {HttpService} from "../../../lib/services/http.service";
import {FoodModel} from "./food.model";
import {Observable, Subject} from "rxjs";
import {ApiConstant} from "../../../lib/constants/api.constant";


@Injectable()
export class FoodService extends FTBaseService {
  dataModel: FoodModel = new FoodModel();

  serviceApi: string = '/mylunch/food';
  url: string = '/mylunch/upload';
  getImageUrl: string = '/mylunch/display/';


  progress: number;
  progressObserver = new Subject();
  progress$ = new Subject();


  // private role:string=ApiConstant.USER_MANAGEMENT +'/role-module';

  constructor(httpService: HttpService) {
    super(httpService);
    this.progress$ = Observable.create(observer => {
      this.progressObserver = observer
    }).share();
  }

  uploadRequest(uploadFile: File): Observable<any> {
    return Observable.create(observer => {
      let formData: FormData = new FormData(),
        xhr: XMLHttpRequest = new XMLHttpRequest();

      formData.append("uploadFile", uploadFile);
      // formData.append("names", names[i]);
      // formData.append("fileTypeName", fileTypeName[i]);


      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            // console.log("[Upload Service] Upload Response: " + xhr.response);
            observer.next(JSON.parse(xhr.response));
            observer.complete();

          } else {
            observer.error(xhr.response);
          }
        }
      };

      xhr.upload.onprogress = (event) => {
        this.progress = Math.round(event.loaded / event.total * 100);

        this.progressObserver.next(this.progress);
        // this.getCancelBtnSubject().subscribe((status: boolean) => {
        //   if (status) {
        //     xhr.abort();
        //   }
        // });
      };
      // console.log("[Upload Service] Form Data: " + JSON.stringify(formData));
      xhr.open('POST', ApiConstant.BASE_API + this.url, true);
      // xhr.setRequestHeader("authorization", this.localStorageService.getToken());
      xhr.send(formData);
    });
  }


}
