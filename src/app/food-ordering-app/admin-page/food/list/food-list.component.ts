import {Component, OnInit} from "@angular/core";
import {PrimeNGListBaseComponent} from "../../../../lib/components/base-form/primeng-list-base.component";
import {FoodService} from "../food.service";
import {FoodModel} from "../food.model";
import {ConfirmationService} from "primeng/components/common/confirmationservice";
import {ApiConstant} from "../../../../lib/constants/api.constant";
import {OverlayPanel} from "primeng/components/overlaypanel/overlaypanel";

@Component({
  selector: 'app-food-list',
  templateUrl: 'food-list.component.html',
  styleUrls: ['food-list.component.css']
})
export class FoodListComponent extends PrimeNGListBaseComponent implements OnInit {
  formHeader: string = "Food Form";
  selectedImageName: string;


  constructor(private foodService: FoodService, protected confirmationService: ConfirmationService) {
    super(confirmationService);
    // this.listData=[
    //   {id:1,code:'R001',name:'Chicken Momo',price:'100.00',description:'Chicken Momo with soup',checked:false,version:0},
    //   {id:2,code:'R002',name:'Buff Momo',price:'100.00',description:'Buff Momo with soup',checked:false,version:0}
    // ];

    this.cols = [
      {field: 'code', header: 'Code'},
      {field: 'name', header: 'Name'},
      {field: 'price', header: 'Price'},
      {field: 'description', header: 'Description'},
    ];
  }

  getService() {
    return this.foodService;
  }

  getNewDataModel() {
    return new FoodModel();
  }

  setSelectedImage(image) {
    if (image) {
      this.selectedImageName = image;
    }
  }

  viewImage(event, selectedFood, overlayImage: OverlayPanel) {
    if (selectedFood.image) {
      this.selectedImageName = selectedFood.image;
      overlayImage.toggle(event);
    }
  }

  getImage() {
    return ApiConstant.IMAGE_DISPLAY + this.selectedImageName;
  }

}



