import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {CustomMaterialModule} from "../../components/module/custom-material.module";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {ManageOrderComponent} from "./manage-order.component";
import {ManageOrderRoutingModule} from "./manage-order-routing.module";
import {OrderDetailDialogComponent} from "./order-detail/order-detial-dialog.component";
import {CustomPrimeNGModule} from "../../components/module/custom-primeng.module";


@NgModule({
  imports: [
    CommonModule,
    ManageOrderRoutingModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CustomPrimeNGModule

  ],
  declarations: [
    ManageOrderComponent,
    OrderDetailDialogComponent
  ],

  entryComponents: [OrderDetailDialogComponent]
})
export class ManageOrderModule {
}
