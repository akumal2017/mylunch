import {Component, OnInit} from "@angular/core";
import {PrimeNGListBaseComponent} from "../../../lib/components/base-form/primeng-list-base.component";
import {OrderService} from "../../service/order.service";
import {ConfirmationService} from "primeng/components/common/confirmationservice";
import {OrderModel} from "../../model/order.model";
import {OrderDetailService} from "../../service/order-detail.service";
import {MenuItem} from "primeng/components/common/menuitem";
import {OrderStatus} from "../../enums/order-status.enum";
import {ResponseModel} from "../../../lib/model/response.model";
import {AlertMessage} from "../../../lib/model/alert-msg.model";
import {AlertType} from "../../../lib/enums/alert-type.enum";
import * as Stomp from "stompjs";
import * as SockJS from "sockjs-client";
import {ApiConstant} from "../../../lib/constants/api.constant";

@Component({
  selector: 'app-manage-order',
  templateUrl: 'manage-order.component.html',
  styleUrls: ['manage-order.component.css']
})
export class ManageOrderComponent extends PrimeNGListBaseComponent implements OnInit {

  formHeader: string = "ORDER DETAIL";
  selectedOrderDetails = [];
  actionItems: MenuItem[];
  selectedOrderId: number;
  disableCloseSaleBtn: boolean = true;

  private serverUrl = ApiConstant.API_ROOT_URL + '/socket';
  private stompClient;

  constructor(private _orderService: OrderService,
              private _confirmationService: ConfirmationService,
              private _orderDetailService: OrderDetailService) {
    super(_confirmationService);
    this.initTableHeaders();
    this.initActionItems();
    this.initializeWebSocketConnection();
  }

  getAll() {
    this.loadingStatus = true;
    this.getService().getList().then((res: ResponseModel) => {
        this.loadingStatus = false;
        if (res.responseStatus) {
          this.listData = res.result;
          if (this.listData.length != 0) {
            this.setDisableCloseSaleBtn(false);
          } else {
            this.setDisableCloseSaleBtn(true);
          }
        } else {
          this.listData = [];
        }
      }
    );
  }

  initializeWebSocketConnection() {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe("/order", (message) => {
        if (message.body) {
          let resOrder = JSON.parse(message.body);
          that.listData.push(resOrder);
          that._sortByOrderDate();
          let alerMsg: AlertMessage = new AlertMessage();
          alerMsg.severity = AlertType.INFO;
          alerMsg.summary = "Order Notification";
          alerMsg.detail = 'Your have a new order, ------------------- Order Code: ' + resOrder.code + ' --------------- Order By: ' + resOrder.orderBy;
          that.showMsg(alerMsg);
          that.setDisableCloseSaleBtn(false);
        }
      });
      }, function (message) {
        if (message.includes('Whoops')) {
          setTimeout(() => {
            that.initializeWebSocketConnection();
          }, 5000);

        }
      }
    );


  }

  private _sortByOrderDate() {
    this.listData.sort((data1, data2) => 0 - ( data1['orderDate'] > data2['orderDate'] ? 1 : -1));
  }


  initTableHeaders() {
    this.cols = [
      {field: 'code', header: 'Order ID'},
      {field: 'orderBy', header: 'Order By'},
      {field: 'totalAmount', header: 'Total Price'},
      {field: 'deliveryType', header: 'Delivery Type'},
      {field: 'orderStatus', header: 'Status'},
    ];
  }

  setDisableCloseSaleBtn(value) {
    this.disableCloseSaleBtn = value;
  }


  initActionItems() {
    this.actionItems = [
      {
        label: 'Delivered', command: () => {
        this.updateOrderStatus(OrderStatus.DELIVERED);
      }
      },
      {
        label: 'In Progress', command: () => {
        this.updateOrderStatus(OrderStatus.IN_PROGRESS);
      }
      },
      {
        label: 'Canceled', command: () => {
        this.updateOrderStatus(OrderStatus.CANCELED);
      }
      }
    ];
  }

  paid(order) {
    this.selectedOrderId = order.id;
    this.updateOrderStatus(OrderStatus.PAID);
  }

  getService() {
    return this._orderService;
  }

  getNewDataModel() {
    return new OrderModel();
  }


  viewOrderDetail(selectedOrder) {
    this.setDisplayDialog(true);
    this.selectedOrderDetails = selectedOrder;
  }

  updateOrderStatus(orderStatus: string) {
    let updateOrder = {orderId: this.selectedOrderId, orderStatus: orderStatus};
    this._orderService.updateByOrderStatus(updateOrder).then((res: ResponseModel) => {
      let alertMsg: AlertMessage = new AlertMessage();
      alertMsg.detail = res.message;
      if (res.responseStatus) {
        alertMsg.summary = "Success";
        alertMsg.severity = AlertType.SUCCESS;
        this.getAll();
      } else {
        alertMsg.severity = "Error";
        alertMsg.severity = AlertType.ERROR;
      }
      this.showMsg(alertMsg);

    });
  }

  onDropdownClick(order) {
    this.selectedOrderId = order.id;
  }

  closeSales() {

    this._orderService.closeSales().then((res: ResponseModel) => {
      let alerMsg: AlertMessage = new AlertMessage();
      if (res.responseStatus) {
        alerMsg.severity = AlertType.SUCCESS;
        alerMsg.summary = "SALES CLOSE";
        alerMsg.detail = 'Sales for today is closed, successfully.';
        this.getAll();
        this.setDisableCloseSaleBtn(true);
      } else {
        this.setDisableCloseSaleBtn(false);
        alerMsg.severity = AlertType.ERROR;
        alerMsg.summary = "SALES CLOSE";
        alerMsg.detail = res.message;
      }
      this.showMsg(alerMsg);
    });

  }

}



