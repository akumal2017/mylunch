import {Component, OnInit} from "@angular/core";
import {AdminDashboardService} from "./admin-dashboard.service";
import {ResponseModel} from "../../../lib/model/response.model";

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: 'admin-dashboard.component.html',
})
export class AdminDashboardComponent implements OnInit {

  totalOrder: number;
  totalSales: number;
  totalDelivered: number;
  totalPickWhenReady: number;

  constructor(private _adminDashboardService: AdminDashboardService) {

  }
  ngOnInit() {
    this.getOrderCount();
  }

  getOrderCount() {
    this._adminDashboardService.orderCount().then((res: ResponseModel) => {
      if (res.responseStatus) {
        let orderCount = res.result;
        this.totalOrder = orderCount.totalOrder;
        this.totalSales = orderCount.totalSales;
        this.totalDelivered = orderCount.totalDelivered;
        this.totalPickWhenReady = orderCount.totalPickWhenReady;
      }
    });
  }

}
