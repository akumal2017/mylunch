import {Injectable} from "@angular/core";
import {FTBaseService} from "../../../lib/services/ft-base.service";
import {HttpService} from "../../../lib/services/http.service";


@Injectable()
export class AdminDashboardService extends FTBaseService {
  // dataModel: FoodModel = new FoodModel();

  serviceApi: string = '/mylunch/admin';

  constructor(httpService: HttpService) {
    super(httpService);

  }

  orderCount() {
    return this.httpService.getRequest(this.serviceApi + '/ordercount');
  }


}
