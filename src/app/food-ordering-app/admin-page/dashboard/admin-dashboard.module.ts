import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AdminDashboardRoutingModule} from "./admin-dashboard-routing.module";
import {AdminDashboardComponent} from "./admin-dashboard.component";
import {AdminDashboardService} from "./admin-dashboard.service";


@NgModule({
  imports: [
    CommonModule,

    AdminDashboardRoutingModule,

  ],
  declarations: [
    AdminDashboardComponent,
  ],
  providers: [AdminDashboardService]
})
export class AdminDashboardModule {
}
