import {OnInit, Component} from "@angular/core";
import {Router} from "@angular/router";
import {InternalRouteConstant} from "../constant/internal-route.constant";
import * as jQuery from "jquery";
import {LoginService} from "../login/login.service";
import {FormAction} from "../../lib/enums/form-action.enum";
import {AlertType} from "../../lib/enums/alert-type.enum";
import {AlertMessage} from "../../lib/model/alert-msg.model";
import {Message} from "primeng/components/common/message";
import {ChangePasswordModel} from "./change-password/change-password.model";
/**
 * Created by admin on 4/15/2018.
 */
@Component({
  selector: 'app-admin-layout',
  templateUrl: 'admin-layout.component.html'
})
export class AdminLayoutComponent implements OnInit {
  isVisible = false;
  msgs: Message[] = [];
  topPosition = 80;
  formHeader: string = "Give Header Name";
  displayDialog: boolean = false;
  buttonType: string;
  dataModel: ChangePasswordModel;
  alertModel: AlertMessage;

  constructor(private _router: Router, private _loginService: LoginService) {
    this.dataModel = new ChangePasswordModel();
    this.alertModel = new AlertMessage();
  }


  ngOnInit(): void {

  }

  onLogoClick() {
    this._router.navigate([InternalRouteConstant.ADMIN_DASHBOARD_ROUTE]);
  }

  onCartClick() {
    this._router.navigate([InternalRouteConstant.MANAGE_ORDER_ROUTE]);
  }

  hideSideNavBar() {
    jQuery("#app-drawer").removeClass('is-visible').attr('aria-hidden', true);
    jQuery(".mdl-layout__drawer-button").attr('aria-expanded', false);
    jQuery(".mdl-layout__obfuscator").removeClass('is-visible');

  }

  logout() {
    this._loginService.logout();
  }

  hideDialog(event) {
    this.setDisplayDialog(false);
  }

  setDisplayDialog(dialogValue: boolean) {
    this.displayDialog = dialogValue;
  }


  onFormSubmit(event) {
    this.setDisplayDialog(event.dialogStatus);
    if (FormAction.SAVE == event.formAction || FormAction.UPDATE == event.formAction) {
      this.alertModel.detail = event.data.message;
      this.alertModel.severity = AlertType.SUCCESS;
      this.showMsg(this.alertModel);
    }
    //this.getService().dataModel = this.getNewDataModel();
    // if (FormAction.CANCEL !== event.formAction) {
    //   this.getAll();
    // }
  }


  showMsg(alertModel: AlertMessage) {
    this.msgs = [];
    this.msgs.push(alertModel);
  }

  changePassword() {
    this.setDisplayDialog(true);
  }


}
