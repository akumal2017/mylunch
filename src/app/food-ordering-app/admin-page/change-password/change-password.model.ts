import {BaseFtModel} from "../../../lib/model/base-ft.model";
/**
 * Created by admin on 5/23/2018.
 */
export class ChangePasswordModel extends BaseFtModel {
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
}
