import {Component, Input} from "@angular/core";
import {PrimeNGFormComponentBase} from "../../../lib/components/base-form/primng-form-base.component";
import {ChangePasswordModel} from "./change-password.model";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {LoginService} from "../../login/login.service";
import {FormAction} from "../../../lib/enums/form-action.enum";
@Component({
  selector: 'app-change-password-form',
  templateUrl: 'change-password-form.component.html',
  styleUrls: ['change-password-form.component.css']
})
export class ChangePasswordFormComponent extends PrimeNGFormComponentBase {
  @Input("changePasswordModel")
  changePasswordModel: ChangePasswordModel;
  @Input("buttonType")
  buttonType: string = this.ADD_BUTTON_ROLE;
  changePasswordForm: FormGroup;
  formErrorMsg: string;


  constructor(protected _formBuilder: FormBuilder, private _loginService: LoginService) {
    super();
    // this.changePasswordModel = new ChangePasswordModel();
  }


  initForm() {
    // this.dataModel = (this.changePasswordModel == null)? new ChangePasswordModel(): this.changePasswordModel;

    this.changePasswordForm = new FormGroup({});
    this.changePasswordForm = this._formBuilder.group({
      oldPassword: [this.changePasswordModel.oldPassword, [Validators.required]],
      newPassword: [this.changePasswordModel.newPassword, [Validators.required]],
      confirmPassword: [this.changePasswordModel.confirmPassword, [Validators.required]],
      version: this.changePasswordModel.version
    });
  }


  submitForm(dialogStatus) {
    if (this.changePasswordForm.valid) {
      this.changePasswordModel = this.changePasswordForm.value;

      this.getService().changePassword(this.changePasswordModel).then(data => {
        if (data.responseStatus) {
          this.emitDialogStatus(dialogStatus, FormAction.UPDATE, data);
          // this.getService().alertModel.alertType = AlertType.SUCCESS;
          this.getService().buttonRole = this.ADD_BUTTON_ROLE;
          this.formErrorMsg = "";

          //this.viewList();
        }
        else {
          console.error("Form update error");
          this.formErrorMsg = data.message;
          // this.alertModel.showAlert = true;
          // this.alertModel.message = data.message;
          // this.alertModel.alertType = AlertType.ERROR;
        }
      });
    }
  }

  getService() {
    return this._loginService;
  }

  isPasswordMatched() {
    let newPwd = this.changePasswordForm.controls['newPassword'].value;
    let confirmPwd = this.changePasswordForm.controls['confirmPassword'].value;
    return (newPwd === confirmPwd);
  }


}




