import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {AdminLayoutComponent} from "./admin-layout.component";

const routes: Routes = [
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {path: '', redirectTo: 'dashboard'},
      {path: 'dashboard', loadChildren: './dashboard/admin-dashboard.module#AdminDashboardModule'},
      {path: 'manage-order', loadChildren: './manage-order/manage-order.module#ManageOrderModule'},
      {path: 'food', loadChildren: './food/food.module#FoodModule'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminLayoutRoutingModule {
}
