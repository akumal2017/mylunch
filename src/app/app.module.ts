import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {AppComponent} from "./app.component";
import {CommonModule} from "@angular/common";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RequestInterceptor} from "./lib/interceptor/request.interceptor";
import {HttpService} from "./lib/services/http.service";
import {AppRoutingModule} from "./app-routing.module";
import {CustomMaterialModule} from "./food-ordering-app/components/module/custom-material.module";
import {CustomPrimeNGModule} from "./food-ordering-app/components/module/custom-primeng.module";
import {SessionStorageService} from "./lib/services/session-storage.service";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    CustomMaterialModule,
    CustomPrimeNGModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: RequestInterceptor,
    multi: true
  },
    HttpService,
    SessionStorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
