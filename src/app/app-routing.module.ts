import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";

const routes: Routes = [
  {path: '', loadChildren: './food-ordering-app/layout.module#LayoutModule'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
//   providers:[
//     {provide: APP_BASE_HREF, useValue: '.'},
//
//     {provide: LocationStrategy, useClass: SorsHashLocationStrategy}
// ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
