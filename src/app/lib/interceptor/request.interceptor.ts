import {Injectable} from "@angular/core";
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent} from "@angular/common/http";
import {Observable} from "rxjs";
import {SessionStorageService} from "../services/session-storage.service";
/**
 * Created by admin on 4/18/2018.
 */
@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  constructor(private _sessionStorageService: SessionStorageService) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authToken = this._sessionStorageService.getToken();
    req = req.clone({
      setHeaders: {
        authorization: authToken !== null ? authToken : "",
      }
    });
    return next.handle(req);
  }

}
