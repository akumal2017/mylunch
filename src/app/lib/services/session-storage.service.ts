/**
 * Created by admin on 5/30/2018.
 */
export class SessionStorageService {
  public static TOKEN_KEY: string = 'appToken';
  // Save data to sessionStorage
  setToken(token) {
    sessionStorage.setItem(SessionStorageService.TOKEN_KEY, token);
  }

  getToken() {
    return sessionStorage.getItem(SessionStorageService.TOKEN_KEY);
  }

  removeToken() {
    sessionStorage.removeItem(SessionStorageService.TOKEN_KEY);
  }

  clearSession() {
    sessionStorage.clear();
  }


}


