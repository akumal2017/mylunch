import {HttpService} from "./http.service";
import {AlertMessage} from "../model/alert-msg.model";

/**
 * Created by anil on 8/4/17.
 */
export class FTBaseService {
  dataModel = {};
  // roleModel:RoleModel = new RoleModel();
  buttonRole: string;
  // status:boolean=false;
  // message:string;
  // show:boolean=false;
  /** example: '/role-module' */
    // protected serviceApi:string;
  alertMessage: AlertMessage;

  protected serviceApi: string;

  constructor(protected httpService: HttpService) {
    this.buttonRole = "addButton";
    this.alertMessage = new AlertMessage();
  }

  getList() {
    return this.httpService.getRequest(this.serviceApi + '/list');
  }

  getAll(currentPage, pageSize, data) {
    return this.httpService.postRequest(this.serviceApi + '/search/' + currentPage + '/' + pageSize, data);
  }

  getByID(id: number) {
    return this.httpService.getRequest(this.serviceApi + '/' + id);
  }

  add(data) {
    return this.httpService.postRequest(this.serviceApi + '/create', data);
  }

  search(searchParam, currentPage, pageSize) {
    return this.httpService.postRequest(this.serviceApi + '/search/' + currentPage + '/' + pageSize, searchParam);
  }


  update(data) {
    return this.httpService.putRequest(this.serviceApi + '/update', data);
  }

  delete(id: number) {
    return this.httpService.deleteRequest(this.serviceApi + '/' + id);
  }

}
