import {HttpClientModule} from "@angular/common/http";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {PageHeaderComponent} from "./components/page-header.component";
/**
 * Created by admin on 4/16/2018.
 */
@NgModule({
  declarations: [
    PageHeaderComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  exports: [PageHeaderComponent],
  providers: []

})
export class FtLibModule {
}
