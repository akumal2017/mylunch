import {Component, OnInit, Input, Output, EventEmitter} from "@angular/core";
/**
 * Created by admin on 4/18/2018.
 */
@Component({
  selector: 'app-page-header',
  templateUrl: 'page-header.component.html',
  styleUrls: ['page-header.component.css'],
})
export class PageHeaderComponent implements OnInit {
  @Input("pageHeaderName")
  pageHeaderName: string = "Please Override header name";
  @Output()
  buttonClick: EventEmitter<any> = new EventEmitter();

  ngOnInit(): void {

  }

  onButtonClick(buttonName: string) {
    this.buttonClick.emit({buttonName: buttonName});
  }


}
