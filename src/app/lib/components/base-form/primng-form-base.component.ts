import {OnInit, Output, EventEmitter} from "@angular/core";
import {FormGroup} from "@angular/forms";
import {FormAction} from "../../enums/form-action.enum";
import {FormsBaseComponent} from "./forms-base.component";
/**
 * Created by admin on 5/23/2018.
 */
export class PrimeNGFormComponentBase extends FormsBaseComponent implements OnInit {

  buttonType: string = this.ADD_BUTTON_ROLE;
  dataModel = {};
  baseForm: FormGroup;
  @Output()
  onFormSubmit: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.initForm();
  }

  constructor() {
    super();
  }

  getService() {
    console.error("Override this method and return the proper service.")
    return null;
  }

  add(dialogStatus) {
    // this.role = this.baseForm.value;
    this.dataModel = this.baseForm.value;
    this.getService().add(this.dataModel).then(data => {
      if (data.responseStatus) {
        let formAction = (dialogStatus === true) ? FormAction.SAVE_AND_NEW : FormAction.SAVE;
        this.emitDialogStatus(dialogStatus, formAction, data);
        this.baseForm.reset();
      }
      else {
        //this.alertModel.alertType = AlertType.ERROR;
        console.error("Form Submit Error");
      }
    });
    // this.getService().add(this.dataModel)
    //   .subscribe(
    //     data => {
    //
    //       if (data.responseStatus) {
    //         let formAction = (dialogStatus === true) ? FormAction.SAVE_AND_NEW : FormAction.SAVE;
    //         this.emitDialogStatus(dialogStatus, formAction, data);
    //         this.baseForm.reset();
    //       }
    //       else {
    //         //this.alertModel.alertType = AlertType.ERROR;
    //       }
    //
    //     }
    //   )
  }

  update(dialogStatus) {
    this.dataModel = {};
    this.dataModel = this.baseForm.value;

    this.getService().update(this.dataModel).then(data => {
      if (data.responseStatus) {
        this.emitDialogStatus(dialogStatus, FormAction.UPDATE, data);
        // this.getService().alertModel.alertType = AlertType.SUCCESS;
        this.getService().buttonRole = this.ADD_BUTTON_ROLE;
        //this.viewList();
      }
      else {
        console.error("Form update error");
        // this.alertModel.showAlert = true;
        // this.alertModel.message = data.message;
        // this.alertModel.alertType = AlertType.ERROR;
      }
    });
  }

  emitDialogStatus(dialogStatus, formAction, data) {
    this.onFormSubmit.emit({dialogStatus: dialogStatus, formAction: formAction, data: data});
  }

  closeForm(dialogStatus) {
    this.emitDialogStatus(dialogStatus, FormAction.CANCEL, {});
  }

  submitForm(dialogStatus: boolean) {
    if (this.baseForm.valid) {
      if (this.buttonType == this.UPDATE_BUTTON_ROLE) {
        this.update(dialogStatus);
      }
      else {
        // this.getService().alertModel.showAlert = false;
        this.add(dialogStatus);
      }
    }
  }

  initForm() {
    console.error("Override this method and initilize the form")
    return null;
  }

}
