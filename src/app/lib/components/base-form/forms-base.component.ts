import {AlertMessage} from "../../model/alert-msg.model";
/**
 * Created by anil on 12/19/17.
 */
export class FormsBaseComponent {
  ADD_BUTTON_ROLE = 'addButton';
  UPDATE_BUTTON_ROLE = 'editButton';
  DEFAULT_DATE_FORMAT = 'yy-mm-dd';
  alertModel: AlertMessage = new AlertMessage();

  constructor() {
  }
}
