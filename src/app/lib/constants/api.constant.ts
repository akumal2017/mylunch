/**
 * Created by anil on 4/18/17.
 */

export class ApiConstant {

  /* public static SORS_ROOT_URL = 'http://localhost:4200';
  public static API_ROOT_URL: string = 'http://localhost:8080'; */

  public static SORS_ROOT_URL = 'http://merolunch.kumal.de';
  public static API_ROOT_URL: string = 'http://apimerolunch.kumal.de';

  public static BASE_API: string = ApiConstant.API_ROOT_URL + '/api';
  public static IMAGE_BASE_API: string = ApiConstant.API_ROOT_URL + '/api/mylunch';
  public static IMAGE_DISPLAY: string = ApiConstant.IMAGE_BASE_API + "/display/";


}
