import {FormsModule, ReactiveFormsModule} from "@angular/forms";
// import {TableModule} from "primeng/components/table/table";
// import {SliderModule} from "primeng/components/slider/slider";
// import {DialogModule} from "primeng/components/dialog/dialog";
// import {MultiSelectModule} from "primeng/components/multiselect/multiselect";
// import {DropdownModule} from "primeng/components/dropdown/dropdown";
// import {ButtonModule} from "primeng/components/button/button";
// import {InputTextModule} from "primeng/components/inputtext/inputtext";
// import {TabViewModule} from "primeng/components/tabview/tabview";
// import {TooltipModule} from "primeng/components/tooltip/tooltip";
// import {GrowlModule} from "primeng/components/growl/growl";
// import {MessageModule} from "primeng/components/message/message";
// import {InputTextareaModule} from "primeng/components/inputtextarea/inputtextarea";
import {NgModule} from "@angular/core";
import {FtLibModule} from "../ft-lib.module";
/**
 * Created by admin on 4/18/2018.
 */
@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    // TableModule,
    // SliderModule,
    // DialogModule,
    // MultiSelectModule,
    // DropdownModule,
    // ButtonModule,
    // InputTextModule,
    // TabViewModule,
    // TooltipModule,
    // GrowlModule,
    // MessageModule,
    // InputTextareaModule,
    FtLibModule

  ],

  exports: [
    FormsModule,
    ReactiveFormsModule,
    // TableModule,
    // SliderModule,
    // DialogModule,
    // MultiSelectModule,
    // DropdownModule,
    // ButtonModule,
    // InputTextModule,
    // TabViewModule,
    // TooltipModule,
    // GrowlModule,
    // MessageModule,
    // InputTextareaModule,
    FtLibModule
  ]

})
export class CommonFormGroupModule {
}
