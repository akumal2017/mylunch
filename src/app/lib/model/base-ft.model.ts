export class BaseFtModel {
  id: number;
  name: string;
  recordState: string;
  version: number;
  checked: boolean = false;

}
