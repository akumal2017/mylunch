/**
 * Created by admin on 5/23/2018.
 */
export class AlertMessage {
  severity: string;
  //title of message
  summary: string;
  detail: string;
}
